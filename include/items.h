#ifndef ITEMS_H
#define ITEMS_H

#include <QPen>
#include <QColor>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

class ColorProperty
{
public:
	virtual QColor getColor() const { return color_; }
	virtual void setColor(const QColor& color) { color_ = color; }
protected:
	QColor color_;
};


class LineItem : public QGraphicsLineItem, public ColorProperty
{
public:
	QColor getColor() const { return pen().color(); }
	void setColor(const QColor& color) { 
		color_ = color; 
		QPen p = pen();
		p.setColor(color_);
		setPen(p); 
	}
};

class RectItem : public QGraphicsRectItem, public ColorProperty
{
public:
	QColor getColor() const { return pen().color(); }
	void setColor(const QColor& color) { 
		color_ = color;
		QPen p = pen();
		p.setColor(color_);
		setPen(p);
	}
};

class EllipseItem : public QGraphicsEllipseItem, public ColorProperty
{
public:
	QColor getColor() const { return pen().color(); }
	void setColor(const QColor& color) { 
		color_ = color;
		QPen p = pen();
		p.setColor(color_);
		setPen(p);
	}
};
#endif