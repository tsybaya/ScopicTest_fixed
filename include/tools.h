#ifndef TOOLS_H
#define TOOLS_H

#include <QGraphicsScene>
#include <QPen>
#include <QPointF>

#include "items.h"

class Tool : public QObject
{
	Q_OBJECT
public:
	virtual ~Tool() = default;

	void setOrigPoint(const QPointF& origPoint) { origPoint_ = origPoint; }
	void setColor(const QColor* color) { color_ = color; }

	virtual void pressMouse(const QPointF& scenePosPoint) = 0;
	virtual void moveMouse(const QPointF& scenePosPoint) = 0;
	virtual void releaseMouse(const QPointF& scenePosPoint) = 0;

	virtual QGraphicsItem* getItem() { return nullptr; }
protected:
	const QColor* color_;
	QPointF origPoint_;
};

class LineTool : public Tool
{
	Q_OBJECT
public:
	LineTool();

	void pressMouse(const QPointF& scenePosPoint) { lineItem = nullptr; }
	void moveMouse(const QPointF& scenePosPoint);
	void releaseMouse(const QPointF& scenePosPoint) {}

	QGraphicsItem* getItem() { return lineItem; }
private:
	LineItem* lineItem;
signals:
	void itemDrawn(QGraphicsItem* item);
};

class RectTool : public Tool
{
	Q_OBJECT
public:
	RectTool();

	void pressMouse(const QPointF& scenePosPoint) { rectItem = nullptr; }
	void moveMouse(const QPointF& scenePosPoint);
	void releaseMouse(const QPointF& scenePosPoint) {}

	QGraphicsItem* getItem() { return rectItem; }
private:
	RectItem* rectItem;
signals:
	void itemDrawn(QGraphicsItem* item);
};

class CircleTool : public Tool
{
	Q_OBJECT
public:
	CircleTool();

	void pressMouse(const QPointF& scenePosPoint) { ellipseItem = nullptr; }
	void moveMouse(const QPointF& scenePosPoint);
	void releaseMouse(const QPointF& scenePosPoint) {}

	QGraphicsItem* getItem() { return ellipseItem; }
private:
	EllipseItem* ellipseItem;
signals:
	void itemDrawn(QGraphicsItem* item);
};

class SelectTool : public Tool
{
	Q_OBJECT
public:
	SelectTool(const QGraphicsScene* scene) { scene_ = scene; num_selected_ = 0; };

	void pressMouse(const QPointF& scenePosPoint);
	void moveMouse(const QPointF& scenePosPoint) {}
	void releaseMouse(const QPointF& scenePosPoint);

private:
	const QGraphicsScene* scene_;

	int num_selected_;
signals:
	void itemsMoved(QList<QGraphicsItem*>& items, QPointF& translation);
};

class PanTool : public Tool
{
	Q_OBJECT
public:
	void pressMouse(const QPointF& scenePosPoint) {}
	void moveMouse(const QPointF& scenePosPoint) {}
	void releaseMouse(const QPointF& scenePosPoint) {}

	QGraphicsItem* getItem() { return nullptr; }
};
#endif // !TOOLS_H
