#ifndef COMMANDS_H
#define COMMANDS_H

#include <QAbstractGraphicsShapeItem>
#include <QUndoCommand>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsView>
#include <QRectF>

#include <QScrollBar>

#include "items.h"

class DrawCommand : public QUndoCommand
{
public:
	DrawCommand(QGraphicsScene *scene, QGraphicsItem* item, QUndoCommand *parent = 0);
	~DrawCommand();

	void undo();
	void redo();
private:
	QGraphicsScene* scene_;
	QGraphicsItem* item_;

	bool isItemBelongToScene;

	void eraseItem();
	void drawItem();
};

class ZoomCommand : public QUndoCommand
{
public:
	ZoomCommand(QGraphicsView *view, double scaleCoef, QUndoCommand *parent = 0);

	void undo();
	void redo();
private:
	QGraphicsView* view_;
	double scaleCoef_;

	void zoom(double scaleCoef);
};

class FitInViewCommand : public QUndoCommand
{
public:
	FitInViewCommand(QGraphicsView* view, QRectF& fitRectF, QUndoCommand *parent = 0);

	void undo();
	void redo();
private:
	QRectF origRectF_;
	QRectF fitRectF_;

	QGraphicsView* view_;

	void fitInRect(QRectF& rectF);
};

class DeleteCommand : public QUndoCommand
{
public:
	DeleteCommand(QGraphicsScene* scene, QList<QGraphicsItem*>& listItem, QUndoCommand *parent = 0);
	~DeleteCommand();

	void undo();
	void redo();
private:
	QGraphicsScene* scene_;
	QList<QGraphicsItem*> listItem_;

	bool isItemsBelongToScene;

	void eraseItems();
	void drawItems();
};

class PanCommand : public QUndoCommand
{
public:
	PanCommand(QGraphicsView* view, QPoint sliderPos, QPoint sliderPosPrev, QUndoCommand *parent = 0);

	void undo();
	void redo();
private:
	QGraphicsView* view_;
	QPoint sliderPos_, sliderPosPrev_;

	void moveSliders(QPoint sliderPos);
};

class MoveCommand : public QUndoCommand
{
public:
	MoveCommand::MoveCommand(QList<QGraphicsItem*>& items, QPointF& translation,
		QUndoCommand *parent = 0);

	void undo();
	void redo();
private:
	QList<QGraphicsItem*> items_;
	
	QVector<QPointF> posMovedItems_;
	QVector<QPointF> origPosMovedItems_;

	void setPositions(QVector<QPointF>& poses);
};

class ColorChangedCommand : public QUndoCommand
{
public:
	ColorChangedCommand(QList<QGraphicsItem*>& items, QColor& color, 
		QUndoCommand *parent = 0);

	void undo();
	void redo();
private:
	QList<QGraphicsItem*> items_;

	QColor color_;
	QVector<QColor> oldColors_;

	void changeColor(QColor& color);
	void changeColor(QVector<QColor>& colors);

	void setOldColors();
};

#endif // !_COMMANDS_H_
