#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QToolBar>

#include <QAction>
#include <QComboBox>
#include <QUndoStack>

#include <QColorDialog>

#include "enumActions.h"
#include "scene.h"
#include "view.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
private:
	Scene* scene;
    View* view;

    void createActions();
    void createConnections();
    void createToolBar();

    QAction* lineAction;
	QAction* rectAction;
	QAction* circAction;

	QAction* zoomInAction;
	QAction* zoomOutAction;
	QAction* zoomAllAction;

    QAction* selectAction;
	QAction* panAction;

	QAction* deleteAction;

	QAction* undoAction;
	QAction* redoAction;

	QUndoStack* undoStack;

    QActionGroup* actionGroup;
    QToolBar* drawingToolBar;

	QComboBox* colorComboBox;

	void initView();
	void initColorComboBox();
public slots:
	void actionGroupClicked(QAction*);
	void changeStateDelete();

	void itemDrawn(QGraphicsItem* lineItem);
	void deleteSelected();
	void zoom(double scaleFactor);
	void zoomAll();
	void pan(QPoint sliderPos, QPoint sliderPosPrev);

	void itemsMoved(QList<QGraphicsItem*>& items,QPointF& translation);

	void itemsColorChanged(QList<QGraphicsItem*>& items, QColor& color);
};

#endif // MAINWINDOW_H
