#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QAction>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QScrollBar>

#include "enumActions.h"
#include "tools.h"

class Scene : public QGraphicsScene
{
	Q_OBJECT
public:
    Scene(QObject* parent = 0);
    void setMode(Mode mode);

	const Tool* getTool() const { return tool_; }
	const Tool* getTool(Mode mode) const;
private:
	Mode sceneMode_;

	Tool* tool_;
	LineTool lineTool_;
	RectTool rectTool_;
	CircleTool circleTool_;
	SelectTool selectTool_;
	PanTool panTool_;
	
	QColor color_;

	void setTool(Mode mode) { tool_=const_cast<Tool*>(static_cast<const Scene*>(this)->getTool(mode)); }
    void makeItemsControllable(bool areControllable);

	void initPenForTools();
protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
signals:
	void itemsColorChanged(QList<QGraphicsItem*>& items, QColor& color);
public slots:
	void changeColor(QString);
};

#endif // SCENE_H
