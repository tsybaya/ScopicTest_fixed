#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QScrollBar>
#include <QMouseEvent>

#include "enumActions.h"

class View : public QGraphicsView
{
	Q_OBJECT
public:
	View(QWidget* parent = 0);
	View(QGraphicsScene* scene, QWidget* parent = 0);

	void setMode(Mode mode);
private:
	Mode mode_;
	const double SCALE_FACTOR_STEP = 1.2;
	const double SCALE_FACTOR_MAX = 15.0;
	const double SCALE_FACTOR_MIN = 0.15;

	double scaleFactor_ = 1.0;

	QPoint slidePosOrig;
protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
signals:
	void zoom(double);
	void pan(QPoint, QPoint);

public slots:
	void zoomIn();
	void zoomOut();
};

#endif // SCENE_H