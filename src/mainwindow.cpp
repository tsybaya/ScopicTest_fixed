#include "mainwindow.h"

const int INIT_SCENE_HEIGHT = 64000;
const int INIT_SCENE_WIDTH = 64000;

MainWindow::MainWindow()
{
    scene = new Scene(this);
    scene->setSceneRect(0.0,0.0,INIT_SCENE_HEIGHT, INIT_SCENE_WIDTH);

	initView();
	initColorComboBox();

	undoStack = new QUndoStack(this);

    createActions();
    createConnections();
    createToolBar();
}

void MainWindow::initView()
{
	view = new View(scene);
	view->setRenderHints(QPainter::Antialiasing);
	setCentralWidget(view);
	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void MainWindow::initColorComboBox()
{
	colorComboBox = new QComboBox;
	QList<QColor> colorList;
	colorList << Qt::black << Qt::red << Qt::green << Qt::blue;

	foreach(QColor colorBox, colorList) {
		QPixmap pixmap(15, 15);
		pixmap.fill(colorBox);
		colorComboBox->addItem(QIcon(pixmap), colorBox.name(), colorBox);
	}
}

void MainWindow::createActions(){
    lineAction = new QAction("Draw line", this);
    lineAction->setData(int(DRAW_LINE));
    lineAction->setIcon(QIcon(":/rsrc/line.png"));
    lineAction->setCheckable(true);

	rectAction = new QAction("Draw Rectangular", this);
	rectAction->setData(int(DRAW_RECT));
	rectAction->setIcon(QIcon(":/rsrc/rect.png"));
	rectAction->setCheckable(true);

	circAction = new QAction("Draw Circle", this);
	circAction->setData(int(DRAW_CIRCLE));
	circAction->setIcon(QIcon(":/rsrc/circ.png"));
	circAction->setCheckable(true);

    selectAction = new QAction("Select object", this);
    selectAction->setData(int(SELECT_OBJECT));
    selectAction->setIcon(QIcon(":/rsrc/select.png"));
    selectAction->setCheckable(true);

	panAction = new QAction("Pan", this);
	panAction->setData(int(PAN));
	panAction->setIcon(QIcon(":/rsrc/pan.png"));
	panAction->setCheckable(true);

	zoomInAction = new QAction("Zoom in", this);
	zoomInAction->setIcon(QIcon(":/rsrc/zoomIn.png"));
	zoomInAction->setCheckable(false);
	
	zoomOutAction = new QAction("Zoom out", this);
	zoomOutAction->setIcon(QIcon(":/rsrc/zoomOut.png"));
	zoomOutAction->setCheckable(false);
	
	zoomAllAction = new QAction("Zoom All", this);
	zoomAllAction->setIcon(QIcon(":/rsrc/zoomAll.png"));
	zoomAllAction->setCheckable(false);

	deleteAction = new QAction("Delete", this);
	deleteAction->setIcon(QIcon(":/rsrc/delete.png"));
	deleteAction->setCheckable(false);
	deleteAction->setDisabled(true);

	undoAction = undoStack->createUndoAction(this, tr("&Undo"));
	undoAction->setIcon(QIcon(":/rsrc/undo.png"));

	redoAction = undoStack->createRedoAction(this, tr("&Redo"));
	redoAction->setIcon(QIcon(":/rsrc/redo.png"));

    actionGroup = new QActionGroup(this);
    actionGroup->setExclusive(true);
    actionGroup->addAction(lineAction);
	actionGroup->addAction(rectAction);
	actionGroup->addAction(circAction);

    actionGroup->addAction(selectAction);
	actionGroup->addAction(panAction);

}

void MainWindow::createConnections(){
    connect(actionGroup, SIGNAL(triggered(QAction*)),
            this, SLOT(actionGroupClicked(QAction*)));

	connect(zoomInAction, SIGNAL(triggered()), 
		view, SLOT(zoomIn()));

	connect(zoomOutAction, SIGNAL(triggered()),
		view, SLOT(zoomOut()));

	connect(zoomAllAction, SIGNAL(triggered()),
		this, SLOT(zoomAll()));

	connect(deleteAction, SIGNAL(triggered()),
		this, SLOT(deleteSelected()));

	connect(scene, SIGNAL(selectionChanged()), 
		this, SLOT(changeStateDelete()));

	connect(scene->getTool(DRAW_LINE), SIGNAL(itemDrawn(QGraphicsItem*)),
		this, SLOT(itemDrawn(QGraphicsItem*)));

	connect(scene->getTool(DRAW_CIRCLE), SIGNAL(itemDrawn(QGraphicsItem*)),
		this, SLOT(itemDrawn(QGraphicsItem*)));

	connect(scene->getTool(DRAW_RECT), SIGNAL(itemDrawn(QGraphicsItem*)),
		this, SLOT(itemDrawn(QGraphicsItem*)));

	connect(view, SIGNAL(zoom(double)),
		this, SLOT(zoom(double)));

	connect(view, SIGNAL(pan(QPoint, QPoint)),
		this, SLOT(pan(QPoint, QPoint)));

	connect(colorComboBox, SIGNAL(activated(QString)),
		scene, SLOT(changeColor(QString)));

	connect(scene->getTool(SELECT_OBJECT), SIGNAL(itemsMoved(QList<QGraphicsItem*>&, QPointF&)),
		this, SLOT(itemsMoved(QList<QGraphicsItem*>&, QPointF&)));

	connect(scene, SIGNAL(itemsColorChanged(QList<QGraphicsItem*>&, QColor&)),
		this, SLOT(itemsColorChanged(QList<QGraphicsItem*>&, QColor&)));
}

void MainWindow::actionGroupClicked(QAction *action){
    scene->setMode(Mode(action->data().toInt()));
	view->setMode(Mode(action->data().toInt()));
}

void MainWindow::createToolBar(){
    drawingToolBar = new QToolBar;
    addToolBar(Qt::TopToolBarArea, drawingToolBar);

    drawingToolBar->addAction(selectAction);
	drawingToolBar->addAction(panAction);
	
    drawingToolBar->addAction(lineAction);
	drawingToolBar->addAction(rectAction);
	drawingToolBar->addAction(circAction);

	drawingToolBar->addAction(zoomInAction);
	drawingToolBar->addAction(zoomOutAction);
	drawingToolBar->addAction(zoomAllAction);

	drawingToolBar->addAction(deleteAction);

	drawingToolBar->addAction(undoAction);
	drawingToolBar->addAction(redoAction);

	drawingToolBar->addWidget(colorComboBox);
}
 
void MainWindow::changeStateDelete()
{
	if (scene->selectedItems().isEmpty())
		deleteAction->setEnabled(false);
	else
		deleteAction->setEnabled(true);
}