#include "commands.h"

DrawCommand::DrawCommand(QGraphicsScene *scene, QGraphicsItem* item,
	QUndoCommand* parent) : QUndoCommand(parent)
{
	scene_ = scene;
	item_ = item;

	isItemBelongToScene = false;
}

DrawCommand::~DrawCommand()
{
	if (!isItemBelongToScene)
		delete item_;
}

void DrawCommand::undo()
{
	eraseItem();
}
void DrawCommand::redo()
{
	drawItem();
}

void DrawCommand::eraseItem()
{
	scene_->removeItem(item_);
	isItemBelongToScene = false;

	scene_->update();
}

void DrawCommand::drawItem()
{
	scene_->addItem(item_);
	isItemBelongToScene = true;

	scene_->update();
}


ZoomCommand::ZoomCommand(QGraphicsView *view, double scaleCoef,
	QUndoCommand *parent) : QUndoCommand(parent)
{
	view_ = view;
	scaleCoef_ = scaleCoef;
}


void ZoomCommand::undo()
{
	zoom(1 / scaleCoef_);
}

void ZoomCommand::redo()
{
	zoom(scaleCoef_);
}

void ZoomCommand::zoom(double scaleCoef)
{
	view_->scale(scaleCoef, scaleCoef);
};


FitInViewCommand::FitInViewCommand(QGraphicsView* view, QRectF& fitRectF,
	QUndoCommand *parent) : QUndoCommand(parent)
{
	view_ = view;
	fitRectF_ = fitRectF;
	origRectF_ = view_->mapToScene(view_->viewport()->geometry()).boundingRect();
}

void FitInViewCommand::undo()
{
	fitInRect(origRectF_);
}

void FitInViewCommand::redo()
{
	fitInRect(fitRectF_);
}

void FitInViewCommand::fitInRect(QRectF& rectF)
{
	view_->fitInView(rectF, Qt::KeepAspectRatio);
}

DeleteCommand::DeleteCommand(QGraphicsScene* scene, QList<QGraphicsItem*>& listItem,
	QUndoCommand *parent) : QUndoCommand(parent)
{
	scene_ = scene;
	listItem_ = listItem;
}
DeleteCommand::~DeleteCommand()
{
	if (!isItemsBelongToScene)
		foreach(QGraphicsItem* item, listItem_)
		delete item;
}

void DeleteCommand::undo()
{
	drawItems();
}
void DeleteCommand::redo()
{
	eraseItems();
}

void DeleteCommand::eraseItems()
{
	foreach(QGraphicsItem* item, listItem_)
		scene_->removeItem(item);

	isItemsBelongToScene = false;

	scene_->update();
}

void DeleteCommand::drawItems()
{
	foreach(QGraphicsItem* item, listItem_)
		scene_->addItem(item);

	isItemsBelongToScene = true;

	scene_->update();
}

PanCommand::PanCommand(QGraphicsView* view, QPoint sliderPos,
	QPoint sliderPosPrev, QUndoCommand *parent) : QUndoCommand(parent)
{
	view_ = view;

	sliderPos_ = sliderPos;
	sliderPosPrev_ = sliderPosPrev;
}

void PanCommand::undo()
{
	moveSliders(sliderPosPrev_);
}

void PanCommand::redo()
{
	moveSliders(sliderPos_);
}

void PanCommand::moveSliders(QPoint sliderPos)
{
	view_->horizontalScrollBar()->setValue(sliderPos.x());
	view_->verticalScrollBar()->setValue(sliderPos.y());
}

MoveCommand::MoveCommand(QList<QGraphicsItem*>& items, QPointF& translation, 
	QUndoCommand *parent) : QUndoCommand(parent)
{
	items_ = items;

	posMovedItems_.resize(items.size());
	origPosMovedItems_.resize(posMovedItems_.size());

	auto pos = posMovedItems_.begin();
	auto origPos = origPosMovedItems_.begin();
	foreach(QGraphicsItem* item, items)
	{
		*pos = item->pos();
		*origPos = *pos - translation;
		++pos, ++origPos;
	}
}

void MoveCommand::undo()
{
	setPositions(origPosMovedItems_);
}

void MoveCommand::redo()
{
	setPositions(posMovedItems_);
}

void MoveCommand::setPositions(QVector<QPointF>& poses)
{
	auto pos = poses.begin();
	foreach(QGraphicsItem* item, items_)
	{
		item->setPos(*(pos));
		++pos;
	}
}

ColorChangedCommand::ColorChangedCommand(QList<QGraphicsItem*>& items, QColor& color, 
	QUndoCommand *parent) : QUndoCommand(parent)
{
	items_ = items;
	color_ = color;

	setOldColors();
}

void ColorChangedCommand::undo()
{
	changeColor(oldColors_);
}
void ColorChangedCommand::redo()
{
	changeColor(color_);
}

void ColorChangedCommand::changeColor(QColor& color)
{
	foreach(QGraphicsItem* item, items_)
		dynamic_cast<ColorProperty*>(item)->setColor(color);
}

void ColorChangedCommand::changeColor(QVector<QColor>& colors)
{
	auto color = colors.begin();
	foreach(QGraphicsItem* item, items_)
	{
		auto temp = dynamic_cast<ColorProperty*>(item);
		dynamic_cast<ColorProperty*>(item)->setColor(*color);
		++color;
	}
}

void ColorChangedCommand::setOldColors()
{
	oldColors_.resize(items_.size());
	auto oldColor = oldColors_.begin();

	foreach(QGraphicsItem* item, items_)
	{
		*oldColor = dynamic_cast<ColorProperty*>(item)->getColor();
		++oldColor;
	}
}