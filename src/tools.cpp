#include "tools.h"


LineTool::LineTool()
{
	lineItem = nullptr;
}

void LineTool::moveMouse(const QPointF& scenePosPoint)
{
	if (!lineItem) {
		lineItem = new LineItem;
		emit itemDrawn(lineItem);
		lineItem->setColor(*color_);
		lineItem->setPos(origPoint_);
	}

	lineItem->setLine(0, 0,
		scenePosPoint.x() - origPoint_.x(),
		scenePosPoint.y() - origPoint_.y());
}

RectTool::RectTool()
{
	rectItem = nullptr;
}

void RectTool::moveMouse(const QPointF& scenePosPoint)
{
	if (!rectItem) {
		rectItem = new RectItem;
		emit itemDrawn(rectItem);
		rectItem->setColor(*color_);
		rectItem->setPos(origPoint_);
	}

	rectItem->setRect(std::min(0.0, scenePosPoint.x() - origPoint_.x()),
		std::min(0.0, scenePosPoint.y() - origPoint_.y()),
		std::abs(scenePosPoint.x() - origPoint_.x()),
		std::abs(scenePosPoint.y() - origPoint_.y()));
}

CircleTool::CircleTool()
{
	ellipseItem = nullptr;
}

void CircleTool::moveMouse(const QPointF& scenePosPoint)
{
	if (!ellipseItem) {
		ellipseItem = new EllipseItem;
		emit itemDrawn(ellipseItem);
		ellipseItem->setColor(*color_);
		ellipseItem->setPos(origPoint_);
	}

	double r = std::sqrt((scenePosPoint.x() - origPoint_.x())*(scenePosPoint.x() - origPoint_.x()) +
		(scenePosPoint.y() - origPoint_.y())*(scenePosPoint.y() - origPoint_.y()));

	ellipseItem->setRect(-r, -r, 2 * r, 2 * r);
}


void SelectTool::pressMouse(const QPointF& scenePosPoint)
{
	num_selected_ = scene_->selectedItems().size();
}

void SelectTool::releaseMouse(const QPointF& scenePosPoint)
{
	if (num_selected_!=0 && origPoint_ != scenePosPoint &&
		num_selected_ == scene_->selectedItems().size())
	{ 
		QPointF translation = scenePosPoint - origPoint_;
		emit itemsMoved(scene_->selectedItems(), translation);
	}

}