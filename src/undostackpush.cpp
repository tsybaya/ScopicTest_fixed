#include "commands.h"
#include "mainwindow.h"

void MainWindow::itemDrawn(QGraphicsItem* item)
{
	undoStack->push(new DrawCommand(scene, item));
}

void MainWindow::zoom(double scaleFactor)
{
	undoStack->push(new ZoomCommand(dynamic_cast<QGraphicsView*>(view), scaleFactor));
}

void MainWindow::zoomAll()
{
	undoStack->push(new FitInViewCommand(view, scene->itemsBoundingRect()));
}

void MainWindow::deleteSelected()
{
	undoStack->push(new DeleteCommand(scene, scene->selectedItems()));
}

void MainWindow::pan(QPoint sliderPos, QPoint sliderPosPrev)
{
	undoStack->push(new PanCommand(view, sliderPos, sliderPosPrev));
}

void MainWindow::itemsMoved(QList<QGraphicsItem*>& items, QPointF& translation)
{
	undoStack->push(new MoveCommand(items, translation));
}

void MainWindow::itemsColorChanged(QList<QGraphicsItem*>& items, QColor& color)
{
	undoStack->push(new ColorChangedCommand(items, color));
}