#include "view.h"

View::View(QWidget* parent) : QGraphicsView(parent)
{
	mode_ = SELECT_OBJECT;
}

View::View(QGraphicsScene* scene, QWidget* parent) : QGraphicsView(scene, parent)
{

}

void View::setMode(Mode mode)
{
	mode_ = mode;

	QGraphicsView::DragMode vMode = QGraphicsView::NoDrag;
	if (mode == DRAW_LINE || mode == DRAW_CIRCLE ||
		mode == DRAW_RECT)
		vMode = QGraphicsView::NoDrag;

	else if (mode == SELECT_OBJECT)
		vMode = QGraphicsView::RubberBandDrag;

	else if (mode == PAN)
		vMode = QGraphicsView::ScrollHandDrag;

	setDragMode(vMode);
}

void View::zoomIn()
{
	if (scaleFactor_ < SCALE_FACTOR_MAX) {
		scaleFactor_ *= SCALE_FACTOR_STEP;
		emit zoom(SCALE_FACTOR_STEP);
	}
}

void View::zoomOut()
{
	if (scaleFactor_ > SCALE_FACTOR_MIN) {
		scaleFactor_ /= SCALE_FACTOR_STEP;
		emit zoom(1/SCALE_FACTOR_STEP);
	}
}

void View::mousePressEvent(QMouseEvent *event)
{
	//if (mode_ == PAN) {
		slidePosOrig.setX(horizontalScrollBar()->value());
		slidePosOrig.setY(verticalScrollBar()->value());
	//}
	QGraphicsView::mousePressEvent(event);
}

void View::mouseReleaseEvent(QMouseEvent *event)
{
	//if (mode_ == PAN)
		if (slidePosOrig.x() != horizontalScrollBar()->value() ||
			slidePosOrig.y() != verticalScrollBar()->value())
			emit pan(QPoint(horizontalScrollBar()->value(), verticalScrollBar()->value()), slidePosOrig);

	QGraphicsView::mouseReleaseEvent(event);
}