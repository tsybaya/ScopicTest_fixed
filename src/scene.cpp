#include "scene.h"
#include "commands.h"

Scene::Scene(QObject* parent) : QGraphicsScene(parent), selectTool_(this)
{
	tool_ = dynamic_cast<Tool*>(&selectTool_);

	initPenForTools();
}

const Tool* Scene::getTool(Mode mode) const
{
	const Tool* tool;

	switch (mode) {
		case DRAW_LINE: 
			tool=dynamic_cast<const Tool*>(&lineTool_);
			break;
		case DRAW_CIRCLE: 
			tool = dynamic_cast<const Tool*>(&circleTool_);
			break;
		case DRAW_RECT: 
			tool = dynamic_cast<const Tool*>(&rectTool_);
			break;
		case SELECT_OBJECT: 
			tool = dynamic_cast<const Tool*>(&selectTool_);
			break;
		case PAN:
			tool = dynamic_cast<const Tool*>(&panTool_);
			break;
		default: 
			tool = nullptr;
	}

	return tool;
}

void Scene::setMode(Mode mode)
{
	sceneMode_ = mode;

	setTool(sceneMode_);

	if (sceneMode_== SELECT_OBJECT)
		makeItemsControllable(true);
	else
		makeItemsControllable(false);
}

void Scene::makeItemsControllable(bool areControllable)
{
	foreach(QGraphicsItem* item, items()) {
		item->setFlag(QGraphicsItem::ItemIsSelectable,
			areControllable);
		item->setFlag(QGraphicsItem::ItemIsMovable,
			areControllable);
	}
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	tool_->setOrigPoint(event->scenePos());
	tool_->pressMouse(event->scenePos());

	QGraphicsScene::mousePressEvent(event);
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	tool_->moveMouse(event->scenePos());

	QGraphicsScene::mouseMoveEvent(event);
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	tool_->releaseMouse(event->scenePos());

	QGraphicsScene::mouseReleaseEvent(event);
}

void Scene::initPenForTools()
{
	lineTool_.setColor(&color_);
	circleTool_.setColor(&color_);
	rectTool_.setColor(&color_);
	selectTool_.setColor(&color_);
}

void Scene::changeColor(QString str)
{
	color_ = QColor(str);
	auto temp = color_.name();

	if (!selectedItems().isEmpty())
		emit itemsColorChanged(selectedItems(), color_);
	else if (tool_->getItem()) {
		QList<QGraphicsItem*> lastCreatedObject{tool_->getItem()};
		emit itemsColorChanged(lastCreatedObject, color_);
	}
}